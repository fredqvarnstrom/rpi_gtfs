import datetime
import threading
import time
from kivy.app import App
from kivy.properties import ObjectProperty, ListProperty
from kivy.clock import Clock
from trip_updater import TripUpdater
from base import Base


class MainApp(App, Base):
    inst_tu = ObjectProperty(None)
    root = ObjectProperty(None)

    def build(self):
        Base.__init__(self)
        self.inst_tu = TripUpdater()
        Clock.schedule_interval(self.display_datetime, 1)

        self.root.ids['lb_route_id'].text = self.get_param_from_xml('ROUTE_ID').split('_')[1]

        threading.Thread(target=self.update_schedule).start()

    def display_datetime(self, *args):
        self.root.ids['lb_cur_time'].text = datetime.datetime.now().strftime("%H:%M:%S")

    def update_schedule(self, *args):
        trip_now = None
        trip_next = None
        while True:
            cur_time = int(time.time())
            if self.inst_tu.update_schedule():
                trip = self.inst_tu.get_timetable_of_bus_stop(
                    self.get_param_from_xml('ROUTE_ID'), self.get_param_from_xml('STOP_ID'))
                print "Trip_now: ", trip_now
                print "Trip_next: ", trip_next
                print "Received: ", trip
                if trip is not None:
                    if trip_now is None:
                        trip_now = trip
                    elif trip['trip_id'] == trip_now['trip_id']:
                        trip_now = trip
                    else:
                        trip_next = trip_now
                        trip_now = trip

            if trip_now is None:
                self.root.ids['lb_now'].text = 'Not available...'
            elif cur_time < trip_now['arrival_time']:
                remaining = trip_now['arrival_time'] - cur_time
                expected = datetime.datetime.now() + datetime.timedelta(seconds=remaining)
                delay = trip_now['arrival_delay']
                if delay > 0:
                    str_delay = str(delay / 60) + 'm ' + str(delay % 60) + 's'
                    tmp = expected.strftime("%H:%M:%S") + " service - " + str_delay + ' late'
                elif delay < 0:
                    str_delay = str(-delay / 60) + 'm ' + str(-delay % 60) + 's'
                    tmp = expected.strftime("%H:%M:%S") + " service - " + str_delay + ' early'
                else:
                    tmp = expected.strftime("%H:%M:%S") + " service - in time"
                self.root.ids['lb_now'].text = tmp
            elif cur_time < trip_now['departure_time']:
                elapsed = cur_time - trip_now['departure_time']
                departured = datetime.datetime.now() - datetime.timedelta(seconds=elapsed)
                self.root.ids['lb_now'].text = departured.strftime("%H:%M:%S") + " service - just arrived..."
            elif cur_time > trip_now['departure_time']:
                elapsed = cur_time - trip_now['departure_time']
                departured = datetime.datetime.now() - datetime.timedelta(seconds=elapsed)
                str_dep = str(elapsed / 60) + 'm ' + str(elapsed % 60) + 's'
                tmp = departured.strftime("%H:%M:%S") + " service - " + str_dep + ' ago'
                self.root.ids['lb_now'].text = tmp

            if trip_next is None:
                self.root.ids['lb_next'].text = ''
            elif cur_time < trip_next['arrival_time']:
                remaining = trip_next['arrival_time'] - cur_time
                expected = datetime.datetime.now() + datetime.timedelta(seconds=remaining)
                delay = trip_next['arrival_delay']
                if delay > 0:
                    str_delay = str(delay / 60) + 'm ' + str(delay % 60) + 's'
                    tmp = expected.strftime("%H:%M:%S") + " service - " + str_delay + ' late'
                elif delay < 0:
                    str_delay = str(-delay / 60) + 'm ' + str(-delay % 60) + 's'
                    tmp = expected.strftime("%H:%M:%S") + " service - " + str_delay + ' early'
                else:
                    tmp = expected.strftime("%H:%M:%S") + " service - in time"
                self.root.ids['lb_next'].text = tmp
            elif cur_time < trip_next['departure_time']:
                elapsed = cur_time - trip_next['departure_time']
                departured = datetime.datetime.now() - datetime.timedelta(seconds=elapsed)
                self.root.ids['lb_next'].text = departured.strftime("%H:%M:%S") + " service - just arrived..."
            elif cur_time > trip_next['departure_time']:
                elapsed = cur_time - trip_next['departure_time']
                departured = datetime.datetime.now() - datetime.timedelta(seconds=elapsed)
                str_dep = str(elapsed / 60) + 'm ' + str(elapsed % 60) + 's'
                tmp = departured.strftime("%H:%M:%S") + " service - " + str_dep + ' ago'
                self.root.ids['lb_next'].text = tmp

            # Repeat after 30 sec
            r = 30 - (time.time() - cur_time)
            if r > 0:
                time.sleep(r)


if __name__ == '__main__':
    app = MainApp()
    app.run()

    # tt = app.get_time_table(stop_id='200916', route_id='2441_389')

    # for row in tt:
    #     print row
